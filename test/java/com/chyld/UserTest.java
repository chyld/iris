package com.chyld;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserTest {
    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testName() throws Exception {
        User u = new User("sue", 21, 105.0);
        assertEquals("sue", u.getName());
    }

    @Test
    public void testAge() throws Exception {
        User u = new User("sue", 21, 105.0);
        assertEquals((Integer)21, u.getAge());
    }

    @Test
    public void testWeight() throws Exception {
        User u = new User("sue", 21, 105.0);
        assertEquals(105.0, u.getWeight(), 0.1);
    }
}
