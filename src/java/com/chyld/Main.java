package com.chyld;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;

public class Main{
  private static final Logger logger = LogManager.getLogger(Main.class);

  public static void main(String[] args){
//      Session s = Data.getSession();
//
//      s.beginTransaction();
//      s.save(new User("ash", 18, 115d));
//      s.getTransaction().commit();

      logger.error("Goodnight, moon 1");
      logger.info("Goodnight, moon 2");
      logger.debug("Goodnight, moon 3");
//      s.close();
      System.exit(0);
  }
}
