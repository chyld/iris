package com.chyld;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="users")
@Access(AccessType.PROPERTY)
public class User {
    // TRY ADDING ANNOTATIONS TO THE FIELDS, NOT THE ACCESSORS
    private int id;
    private String name;
    private Integer age;
    private Double weight;
    private Date createdAt;
    private Integer notInDb;

    @Transient
    public Integer getNotInDb() {
        return notInDb;
    }
    public void setNotInDb(Integer notInDb) {
        this.notInDb = notInDb;
    }

    public User(String name, Integer age, Double weight) {
        this.name = name;
        this.age = age;
        this.weight = weight;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 25)
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "age", nullable = true)
    public Integer getAge() {
        return age;
    }
    public void setAge(Integer age) {
        this.age = age;
    }

    @Basic
    @Column(name = "weight", nullable = true, precision = 0)
    public Double getWeight() {
        return weight;
    }
    public void setWeight(Double weight) {
        this.weight = weight;
    }

    @Basic
    @Column(name = "created-at", updatable = false)
    public Date getCreatedAt() {
        return createdAt;
    }
    public void setCreatedAt(Date ca) {
        this.createdAt = ca;
    }
}
